from PIL import Image
from os import walk


def watermark_photo(input_image_path,
                    output_image_path,
                    watermark_image_path,
                    position):
    base_image = Image.open(input_image_path)
    watermark = Image.open(watermark_image_path)

    # add watermark to your image
    base_image.paste(watermark, position)
    # base_image.show()
    base_image.save(output_image_path)


if __name__ == '__main__':

    for (dirpath, dirnames, filenames) in walk('Images'):
        for image in filenames:
            print(image)
            img = 'Images/lighthouse-300x200.jpg'
            watermark_photo('Images/' + image, 'watermarked/' + image + '_watermarked.jpg',
                                'watermarks/favicon.png', position=(0, 0))
